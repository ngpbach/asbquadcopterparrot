/*
 * C:\Users\Phongbach\MATLAB\Projects\slexamples\asbQuadcopter7\work\slprj\ert\_sharedutils\rt_powf_snf.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "stateEstimator".
 *
 * Model version              : 1.43
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Fri Sep 28 23:19:11 2018
 * Created for block: stateEstimator
 */

#ifndef SHARE_rt_powf_snf
#define SHARE_rt_powf_snf
#include "rtwtypes.h"

extern real32_T rt_powf_snf(real32_T u0, real32_T u1);

#endif
