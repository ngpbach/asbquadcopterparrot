/*
 * const_params.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "flightControlSystem".
 *
 * Model version              : 1.139
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Fri Sep 28 23:19:25 2018
 */
#include "rtwtypes.h"

extern const int32_T rtCP_pooled_JTeu8W9IxEfI[8];
const int32_T rtCP_pooled_JTeu8W9IxEfI[8] = { -1, 121, 122, 123, 1, -121, -122,
  -123 } ;
